#include "mem.h"
#include "mem_internals.h"
#include <stdint.h>
#include <stdio.h>
#ifndef MEMORY_ALLOCATOR_TEST_H
#define MEMORY_ALLOCATOR_TEST_H

#endif //MEMORY_ALLOCATOR_TEST_H

int common_allocate_test(size_t size);

static struct block_header* block_get_header(void* contents);

int free_one_block_test(size_t size);

int free_two_blocks_test(size_t size);

int new_region_expand_old_test(size_t size);

int non_expandable_region_test(size_t size);

void all_tests();
